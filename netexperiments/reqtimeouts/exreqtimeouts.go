package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {

	for _, s := range []string{"http://sex.com", "http://sexxxxxx.com", "http://yahoo.com", "http://swr234s.com", "http://hsppe.jp"} {
		timerun, _, err := reqtime(2, s)
		fmt.Printf("%v %v\n-- time runing: %v\n", s, err, timerun)
	}

	//ex()
	//ex2()
}
func reqtime(t int, u string) (string, string, error) {
	start := time.Now()
	ctx, cf := context.WithTimeout(context.Background(), time.Second*time.Duration(t))
	go func() {
		time.Sleep(time.Second * 5)
		cf()
		return
	}()
	req, err := http.NewRequestWithContext(ctx, "GET", u, nil)
	if err != nil {
		return "", "", err
	}
	cc := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       0,
	}
	resp, err := cc.Do(req)
	if err != nil {
		return "", "", err
	}
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", "", err
	}
	fmt.Printf("-- time running  %v\n", time.Since(start).String())
	return time.Since(start).String(), string(res), err
}
func ex() {
	req, err := http.NewRequest("GET", "http://www.yahoo.co.jp", nil)
	if err != nil {
		log.Fatalf("%v", err)
	}

	ctx, cancel := context.WithTimeout(req.Context(), 1*time.Millisecond)
	defer cancel()

	req = req.WithContext(ctx)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Printf("%v\n", res.StatusCode)
}

func ex2() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	req, _ := http.NewRequestWithContext(ctx, "GET", "http://httpbin.org/delay/3", nil) // it will return later 3 sec

	client := &http.Client{}

	go func() {
		time.Sleep(time.Second * 4)
		println("Cancel")
		cancel()
	}()

	println("Do")
	resp, err := client.Do(req)
	println("Do finished")

	if err != nil {
		panic(err) // cancel caught
	}

	println(resp.StatusCode)
}
