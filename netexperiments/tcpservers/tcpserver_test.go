package tcpserver

import (
	"testing"
)

func TestServer_Run(t *testing.T) {
	s, e := New("localhost", "7777")
	if e != nil {
		t.Error(e)
	}
	s.Run()
}