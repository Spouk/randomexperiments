package tcpserver

import (
	"bufio"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"time"
)

type server struct {
	host, port string
	c          net.Listener
}

func New(host, port string) (*server, error) {

	s := &server{
		host: host,
		port: port,
		c:    nil,
	}
	l, e := net.Listen("tcp4", fmt.Sprintf("%s:%s", host, port))
	if e != nil {
		log.Println(e)
		return nil, e
	}
	s.c = l
	return s, nil
}

func (s *server) Run() {
	log.Println("-- tcp server starting on `" + s.host + ":" + s.port)
	for {
		c, err := s.c.Accept()
		if err != nil {
			log.Println(err)
		}
		go s.worker(c)
	}
}
func (s *server) worker(c net.Conn) {
	defer func() {
		_ = c.Close()
	}()
	log.Printf("-- worker starting %v\n", c.RemoteAddr())
	scan := bufio.NewScanner(c)
	_, _ = c.Write([]byte("=>"))
	for scan.Scan() {
		fc := scan.Text()
		switch fc {
		case "help":
			s.commandHelp(c)
		case "exit":
			_, _ = c.Write([]byte("-- cya later...\n"))
			return
		default:
			if strings.HasPrefix(fc, "connect") {
				res := strings.Split(fc, "'")
				log.Printf("found prefix connect %v %v %v\n", fc, res, len(res))
				if len(res) == 3 {
					//make time out  context
					ctx, cf := context.WithTimeout(context.Background(), time.Second*3)
					req, err := http.NewRequestWithContext(ctx, "GET", res[1], nil)
					if err != nil {
						log.Println(err)
					}
					//run goroutines for awaiting set time  period request expired
					go func() {
						time.Sleep(time.Second * 3)
						cf()
					}()
					//make new http client for make request
					client := http.Client{}
					resp, err := client.Do(req)
					if err != nil {
						log.Println(err)
					} else {
						//read answer http response
						answer, err := ioutil.ReadAll(resp.Body)
						if err != nil {
							log.Println(err)
						} else {
							_, _ = c.Write([]byte(answer))
						}
					}
				}
			}
			_, _ = c.Write([]byte("wrong command, user `help` for show list commands\n"))
		}
	}
}
func (s *server) commandHelp(c net.Conn) {
	_, _ = c.Write([]byte("=[console menu]===================================\n"))
	_, _ = c.Write([]byte(" * exit - exit from client \n"))
	_, _ = c.Write([]byte(" * help  - show this help \n"))
	_, _ = c.Write([]byte(" * connect 'url' - make http request \n"))
	_, _ = c.Write([]byte("=========================================\n"))
	_, _ = c.Write([]byte("=>"))
}
