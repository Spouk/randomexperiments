package main

import (
	"fmt"
	"github.com/gliderlabs/ssh"
	"log"
	"os"
	"strings"
)

func main() {

	r := strings.Split("234234:22", ":")
	fmt.Println(r, len(r))
	os.Exit(1)

	ssh.Handle(func(s ssh.Session) {
		s.Write([]byte("==> "))
		s.Write([]byte(s.User()))
		b := make([]byte, 100)
		s.Read(b)
		fmt.Println(string(b))
	})

	log.Fatal(ssh.ListenAndServe(":2222", nil))
}
