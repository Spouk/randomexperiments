package main

import (
	"fmt"
	"log"
	"randomexperiments/sshex/sshlib"
)

func main() {
	// ssh refers to the custom package above
	conn, err := sshlib.Connect("0.0.0.0:22", "spouk", "spouknet")
	if err != nil {
		log.Fatal(err)
	}

	output, err := conn.SendCommands("sleep 2", "echo Hello!")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(output))
}
