module randomexperiments

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gliderlabs/ssh v0.3.3
	github.com/go-chi/chi/v5 v5.0.4
	github.com/jinzhu/gorm v1.9.16
	github.com/kjx98/go-ncurses v0.0.0-20200412090852-44d1c314d1cf
	github.com/nakagami/firebirdsql v0.9.1
	github.com/pkg/sftp v1.13.2
	gitlab.com/Spouk/gotool v0.0.0-20210814190327-15c61b74cc1e
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
)
