package main

// #cgo LDFLAGS: -lusb
// #include <stdio.h>
// #include <usb.h>
//void hello(){
//    struct usb_bus *bus;
//    struct usb_device *dev;
//    usb_init();
//    usb_find_busses();
//    usb_find_devices();
//    for (bus = usb_busses; bus; bus = bus->next)
//        for (dev = bus->devices; dev; dev = dev->next){
//            printf("Trying device %s/%s\n", bus->dirname, dev->filename);
//            printf("\tID_VENDOR = 0x%04x\n", dev->descriptor.idVendor);
//            printf("\tID_PRODUCT = 0x%04x\n", dev->descriptor.idProduct);
//            printf("\twusb = %s\n", dev->devpath);
//        }
//}
//
import "C"

import "fmt"

func main(){
     fmt.Printf("Hello world\n")
     C.hello()
   }