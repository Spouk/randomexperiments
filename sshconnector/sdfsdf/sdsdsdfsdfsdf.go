package main

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"log"
)

var escapePrompt = []byte{'$', ' '}

func main() {
	config := &ssh.ClientConfig{
		User: "fuckyou",
		Auth: []ssh.AuthMethod{
			ssh.Password("fuckyou"),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	client, err := ssh.Dial("tcp", "127.0.0.1:22", config)
	if err != nil {
		panic(err)
	}

	defer client.Close()
	session, err := client.NewSession()

	if err != nil {
		log.Fatalf("unable to create session: %s", err)
	}
	defer session.Close()

	modes := ssh.TerminalModes{
		ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}

	if err := session.RequestPty("xterm", 80, 40, modes); err != nil {
		log.Fatal(err)
	}

	w, err := session.StdinPipe()
	if err != nil {
		panic(err)
	}
	r, err := session.StdoutPipe()
	if err != nil {
		panic(err)
	}
	if err := session.Start("/bin/sh"); err != nil {
		log.Fatal(err)
	}
	readUntil(r, escapePrompt) //ignore the shell output

	//write(w, "/bin/ls -lhav")

	//out, err := readUntil(r, escapePrompt)

	//fmt.Printf("/bin/ls output: %s\n", *out)

	write(w, "/bin/whoami")

	out, err := readUntil(r, escapePrompt)

	//start sudo
	write(w, "/bin/sudo /bin/docker ls")
	out, err = readUntil(r, escapePrompt)
	write(w, "sdfsdf")
	out, err = readUntil(r, escapePrompt)

	fmt.Printf("%s\n", *out)

	//end sudo

	fmt.Printf("/bin/whoami: %s\n", *out)

	write(w, "exit")

	session.Wait()
}

func write(w io.WriteCloser, command string) error {
	_, err := w.Write([]byte(command + "\n"))
	return err
}

func readUntil(r io.Reader, matchingByte []byte) (*string, error) {
	var buf [64 * 1024]byte
	var t int
	for {
		n, err := r.Read(buf[t:])
		if err != nil {
			return nil, err
		}
		t += n
		if isMatch(buf[:t], t, matchingByte) {
			stringResult := string(buf[:t])
			return &stringResult, nil
		}
	}
}

func isMatch(bytes []byte, t int, matchingBytes []byte) bool {
	if t >= len(matchingBytes) {
		for i := 0; i < len(matchingBytes); i++ {
			if bytes[t-len(matchingBytes)+i] != matchingBytes[i] {
				return false
			}
		}
		return true
	}
	return false
}
