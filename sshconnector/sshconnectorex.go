package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func exctx(ctx context.Context, req string, result chan string) {
	log.Println("--starting function sql requesting...")

	//run worker
	go func(ctx context.Context, ch chan string, req string) {
		if d, err := sql.Open("firebirdsql", "sysdba:sysdba@localhost:3050/tester"); err != nil {
			log.Println(err)
			//ch <- true
			return

		} else {
			r, err := d.QueryContext(ctx, "select 1 from rdb$database;")
			if err != nil {
				log.Println(err)
				return
			}
			log.Println("-- success result read from database during timeout context :", r)
			var i int
			_ = r.Scan(&i)
			ch <- fmt.Sprintf("%d", i)
			return
		}
	}(ctx, result, req)
	//waiting context
	for {
		select {
		case <-ctx.Done():
			log.Println("--found timeout context, need return from requsting...")
			return
		case <-ch:
			log.Println("-- yes, function with context succes working with timeout!! Creating :)")
			return
		}
	}
}

func main() {

	ctx, dc := context.WithTimeout(context.Background(), time.Second*1)
	defer func() {
		log.Println("-- closing root context, cya  later")
		dc()
	}()
	var res = make(chan string)
	go exctx(ctx, "sdf", res)
	for {
		select {
		case <-ctx.Done():
			log.Println("------- AWAITINg")
			return
		case ru := <-res:
			log.Println("-- result getting :", ru)
			return
		}
	}

	os.Exit(1)

	//form := fmt.Sprintf("%v\n" + "%v\n", 'a','s')
	//fmt.Println(form)
	//os.Exit(1)
	var (
		hostport = "localhost:24"
		//port     = "-p24"
		//sshpass  = "sshpass -p %s %s %s"
		version     = "/opt/firebird/bin/firebird -z"
		pass        = "guest"
		checkexist  = "test -f /opt/firebird/plugins/libcluster.so && echo exists"
		checkexist2 = "test -f /opt/firebird/plugins/test && echo exists"
	)

	//ostport, port)
	cc := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            "guest",
		Auth: []ssh.AuthMethod{
			ssh.Password(pass),
		},
		Timeout: time.Second * 7,
	}
	conn, err := ssh.Dial("tcp", hostport, cc)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	//check exist file remote server
	res, err := exessh(conn, checkexist)
	if err != nil {
		//log.Fatal(res, err)
		log.Println("file remote server not found")
	}
	log.Println("result: ", string(res), err)

	//check exist file remote server
	res, err = exessh(conn, checkexist2)
	if err != nil {
		log.Fatal(res, err)
	}
	log.Println("result: ", string(res), err)

	//check version firebird
	res, err = exessh(conn, version)
	if err != nil {
		log.Fatal("check version firebird:", err)
	}
	fmt.Println(string(res))
	if !strings.Contains(string(res), "V3.0") {
		fmt.Println("-- warining : version firebird not correct for install plugins")
		return
	}
	fmt.Println("-- correct version firebird for update new plugins")

	//sending files to remote host
	// open an SFTP session over an existing ssh connection.
	sftp, err := sftp.NewClient(conn)
	if err != nil {
		log.Fatal("sftp make new client error", err)
	}
	defer sftp.Close()

	for _, x := range []string{"/tmp/1.doc", "/tmp/2.doc"} {
		ff, err := os.Open(x)
		if err != nil {
			log.Println(err)
			continue
		}
		_, fname := filepath.Split(x)

		// Create the destination file
		log.Println(fname)
		dstFile, err := sftp.Create(fmt.Sprintf("/tmp/stock/%s", fname))
		if err != nil {
			log.Fatal("--[created output file] ", err)
		}
		defer dstFile.Close()

		// write to file
		if _, err := dstFile.ReadFrom(ff); err != nil {
			log.Fatal(err)
		}
		log.Println("--success sending count bytes ")
	}
}
func exessh(conn *ssh.Client, comm string) (string, error) {
	s, err := conn.NewSession()
	if err != nil {
		return "", err
	}
	defer s.Close()

	res, err := s.CombinedOutput(comm)
	return string(res), err

}
