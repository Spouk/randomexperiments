package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	pp := flag.String("prgname", "", "program name with full path for check  libraries")
	if len(os.Args)-1 != 2 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	flag.Parse()
	var (
		lall []gg
		lnf  []gg
		g    gg
	)
	nfa, err := exec.Command("ldd", *pp).Output()
	errcheck(err)
	for _, x := range strings.Split(string(nfa), "\n") {
		fmt.Println(x)
		x = strings.TrimSpace(x)
		if strings.Contains(x, "=>") {
			t := strings.Split(x, "=>")
			log.Println(t, len(t), strings.Split(t[0], "."))
			g = gg{
				short:  strings.Split(t[0], ".")[0],
				name:   strings.TrimSpace(t[0]),
				status: t[1],
			}
		} else {
			t := strings.Split(x, " ")
			if len(t) >= 2 {
				g = gg{
					short:  strings.Split(t[0], ".")[0],
					name:   strings.TrimSpace(t[0]),
					status: t[1],
				}
			}
		}
		if strings.Contains(x, "not found") {
			lnf = append(lnf, g)
		}
		lall = append(lall, g)
	}
	for _, x := range lnf {
		fmt.Println(x)
	}

	//check  library haved status `not found` in repository
	for _, x := range lnf {
		repo := exec.Command("xbps-query", "-Rs", x.short)
		rep, _ := repo.Output()
		fmt.Println(string(rep), repo.String())
	}

	b := context.Background()
	cc, cf := context.WithTimeout(b, time.Second*2)
	defer cf()
	res := make(chan bool)
	go func() {
		cmd, _ := exec.CommandContext(cc, "ls").Output()
		time.Sleep(time.Second * 3)
		log.Println(string(cmd))
		res <- true
	}()
	select {
	case <-cc.Done():
		log.Println("timeout expired executing console command,exit")
	case <-res:
		log.Println("all ok and done execute console command")
	}
	os.Exit(1)
}
func errcheck(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type gg struct {
	short      string
	name       string
	status     string
	repostatus string
}

func (g gg) String() string {
	return fmt.Sprintf(" * [%s] [%s] => [%s]", g.short, g.name, g.status)
}
