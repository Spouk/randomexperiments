package main

import (
	"crypto"
	"crypto/md5"
	"fmt"
	"hash"
	"strings"
	"time"
)

func main() {
	c := crypto.Hash(crypto.MD5).New()
	c.Write([]byte("test"))

	mh := md5.New()
	//mh.Write([]byte("test"))
	fmt.Println(string(mh.Sum([]byte("tester"))))
	fmt.Printf("%x\n", mh.Sum([]byte("tester")))

	l := New()
	password := "tester"
	hashpassword, salt := l.encode(password)
	fmt.Println(hashpassword, salt, l.decode(password, salt, hashpassword))
	fackedsalt := fmt.Sprintf("%v", time.Now().UnixNano())

	fmt.Println(hashpassword, fackedsalt, l.decode(password, fackedsalt, hashpassword), strings.EqualFold("tester", "testere"))
	hh := NewHasher()
	fmt.Println(hh)

}

type cru struct {
	cc hash.Hash
}

func New() *cru {
	return &cru{
		cc: crypto.Hash(crypto.MD5).New(),
	}
}
func (c *cru) encode(password string) (string, string) {
	c.cc.Reset()
	salt := fmt.Sprintf("%v", time.Now().UnixNano())
	return fmt.Sprintf("%x", c.cc.Sum([]byte(password+salt))), salt
}
func (c *cru) decode(password string, salt string, hashpassword string) bool {
	c.cc.Reset()
	res := fmt.Sprintf("%x", c.cc.Sum([]byte(password+salt)))
	fmt.Println(res, "\n", hashpassword)
	return strings.EqualFold(res, hashpassword)
}

type Hasher struct {
	C hash.Hash
}

func NewHasher() *Hasher {
	return &Hasher{
		C: md5.New(),
	}
}
