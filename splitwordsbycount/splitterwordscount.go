package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	var (
		infile, outfile string
		result              = make(map[int][]string)
		countday        int = 1
		countline       int = 0
	)
	flag.StringVar(&infile, "inf", "", "infile with for example english words")
	flag.StringVar(&outfile, "outf", "", "outfile for save result")
	flag.Parse()
	if len(os.Args[1:]) != 4 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	p, _ := filepath.Abs(infile)
	fh, err := os.Open(p)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := fh.Close()
		if err != nil {
			log.Println(err)
			os.Exit(-1)
		}
	}()
	p, _ = filepath.Abs(outfile)
	fout, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := fout.Close()
		if err != nil {
			log.Println(err)
			os.Exit(-1)
		}
	}()
	w := bufio.NewWriter(fout)
	s := bufio.NewScanner(fh)
	for s.Scan() {
		l := s.Text()
		if len(strings.TrimSpace(l)) > 1 {
			if countline == 10 {
				countline = 0
				countday++
				_, err := w.WriteString(fmt.Sprintf("day %3d\n---\n", countday))
				if err != nil {
					log.Println(err)
				}
			}
			countline++
			result[countday] = append(result[countday], l)
			_, err := w.WriteString(fmt.Sprintf("%s\n", l))
			if err != nil {
				log.Println(err)
			}
		}
	}
	log.Println("-- the end ;) --")
}
